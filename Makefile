output:main.o jobs.o
	g++ -std=c++0x -Wall main.o jobs.o -o simulate

main.o::main.cpp
	g++ -c main.cpp

jobs.o:jobs.cpp jobs.h
	g++ -c jobs.cpp