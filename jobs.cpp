#include "jobs.h"

jobs::jobs()
{
  
}

jobs::~jobs()
{
  
}

void jobs::setJobName(string j)
{
  jobName = j;
}

string jobs::getJobName()const
{
  return jobName;
}

void jobs::setArrival(int a)
{
  arrival = a;
}

int jobs::getArrival()const
{
  return arrival;
}

void jobs::setDuration(int d)
{
  duration = d;
}

int jobs::getDuration()const
{
  return duration;
}
