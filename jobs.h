#pragma once
#include <iostream>
#include <string>

using namespace std;
class jobs
{
 private:


 public:
  string jobName;
  int arrival;
  int duration;
  jobs();  
  ~jobs();

  void setJobName(string j);
  string getJobName()const;

  void setArrival(int a);
  int getArrival()const;

  void setDuration(int d);
  int getDuration()const;

};
