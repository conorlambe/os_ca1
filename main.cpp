#include "jobs.cpp"
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <queue>
#include <vector>

using namespace std;

void readFile(vector<jobs>&);
void printFileVector(const vector<jobs>&);
vector<jobs> getFifo(const vector<jobs>&);


int main()
{
	vector<jobs> jobFile;
	readFile(jobFile);
	printFileVector(jobFile);
	vector<jobs> fifoVec;
	fifoVec.assign(jobFile.begin(), jobFile.end());

	int count = 0, index = 0;
	vector<string> temp;
	int totalDur = 0;

	int total = 0;
	for(int i = 0; i < jobFile.size(); i++){
		total += jobFile[i].getDuration();
	}

	for(int m = 0; m < total; m++){
		
	for(int h = 0; i < fifoVec.size(); h++){
		totalDur+=fifoVec[h].getDuration();
	}
	int each_dur = fifoVec[index].getDuration();
	if(count < jobFile[index].getDuration()){
		temp.push_back(fifoVec[index].getJobName());
		cout++;
		if(count == each_dur){
			index++;
			count=0;
		}
	}
	cout << fifoVec[m].getJobName() << endl;
	}
	return 0;
}

void readFile(vector<jobs>& jobFile)
{
	ifstream inFile("test.txt", ios::in);
	string line;

	string jobName;
	int arrival;
	int duration;


	if(inFile.is_open()){
		getline(inFile, line);
		inFile >> jobName >> arrival >> duration;

		while(inFile.good()) {
			getline(inFile, line);
			inFile >> jobName >> arrival >> duration;

			jobs addJob;
			addJob.setJobName(jobName);
			addJob.setArrival(arrival);
			addJob.setDuration(duration);
			jobFile.push_back(addJob);
		}
	}
	else {
		cout << "Error Opening " << endl;
	}
	inFile.close();
}

void printFileVector(const vector<jobs>& printFile)
{
	cout << "Print File Vector " << endl;
	cout << "-----------------------------------------------" << endl;
	cout << " Job Name \tArrival Time \t\tDuration " << endl;
	cout << "-----------------------------------------------" << endl;

	unsigned int size = printFile.size();
	for (unsigned int i = 0; i < size; i++)
	{
		cout << printFile[i].getJobName() << "\t\t" << printFile[i].getArrival() << "\t\t" << printFile[i].getDuration() << endl; 
	}

}

//FIFO
//vector<jobs> getFifo(vector<job>& jbList){
//	int count = 0, index = 0;
//	vector<string> temp;
//	int totalDur = 0;
//	for(int i = 0; i < jbList.size(); i++){
//		totalDur+=jbList.getDuration();
//	}
//	int each_dur = jbList[index].getDuration();
//	if(count < jobs[index].getDuration()){
//		temp.push_back(jbList[index].getJobName());
//		cout++;
//		if(count == each_dur){
//			index++;
//			count=0;
//		}
//	}
//	return temp;
//}
